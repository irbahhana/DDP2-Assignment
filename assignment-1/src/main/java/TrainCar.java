public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
        this.next = null;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        if(next == null){
            return this.cat.weight + EMPTY_WEIGHT;
        } else{
            return this.cat.weight + EMPTY_WEIGHT + this.next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if(next == null){
            return this.cat.computeMassIndex();
        } else{
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        if(next == null){
            System.out.print("(" + this.cat.name + ")");
        }else{
            System.out.print("(" + this.cat.name +")"+"--");
            this.next.printCar();
            //System.out.println("");
        }
    }
}
