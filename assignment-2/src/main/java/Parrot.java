import java.util.Scanner;

class Parrot extends IndoorCage {

    Parrot(String name, int length) {
        super(name, length);
    }

    //Method action
    public void action(int number) {
        if (number == 1) {
            this.ordertoFly();
        } else if (number == 2) {
            this.doConversation();
        } else {
            System.out.println(this.getName() + " says: HM?");
        }
        System.out.println("Back to the office!\n");
    }

    private void ordertoFly() {
        System.out.println("Parrot" + this.getName() + " flies!\n" +
                this.getName() + " makes a voice: FLYYYY…..");
    }

    private void doConversation() {
        Scanner inputtemp = new Scanner(System.in);
        System.out.print("You say: ");
        String word = inputtemp.nextLine();
        System.out.println(this.getName() + " says: " + word.toUpperCase());
    }
}
