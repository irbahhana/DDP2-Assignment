import java.util.Scanner;
import java.util.ArrayList;

public class JavariPark {

    //Array list consist of object from each type
    private static ArrayList<Animal> listCat = new ArrayList<>();
    private static ArrayList<Animal> listEagle = new ArrayList<>();
    private static ArrayList<Animal> listHamster = new ArrayList<>();
    private static ArrayList<Animal> listParrot = new ArrayList<>();
    private static ArrayList<Animal> listLion = new ArrayList<>();


    public static void main(String[] args) {
        String[] listType = {"cat", "lion", "eagle", "parrot", "hamster"};
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        int listCount[] = new int[5];

        //Input for 5 type of animal
        for (int i = 0; i < 5; i++) {
            System.out.print(listType[i] + ": ");
            int count = Integer.parseInt(input.nextLine());
            listCount[i] = count;
            //Checking if there is a 0 input
            if (count > 0) {

                //Initiate the animal
                JavariPark park = new JavariPark();
                park.initiation(listType[i]);
            }
        }
        System.out.println("Animals have been successfully recorded!");

        
        JavariPark park2 = new JavariPark();
        park2.printArrange();

        //printing animal
        System.out.println("NUMBER OF ANIMALS:");
        for (int k = 0; k < 5; k++){
            System.out.println(listType[k] + ":" + listCount[k]);
        }
        System.out.println("\n");
        System.out.println("=============================================");
        park2.visitAnimal();
    }

    private void initiation(String type) {
        System.out.println("Provide the information of " + type + "(s):");
        Scanner animalInput = new Scanner(System.in);
        String[] listBefore = animalInput.nextLine().split(",");

        //putting each result to their corresponding type
        for (String temp : listBefore) {
            String[] listAfter = temp.split("\\|");
            if (type.equalsIgnoreCase("cat")) {
                Cat kucing = new Cat(listAfter[0], Integer.parseInt(listAfter[1]));
                listCat.add(kucing);
            } else if (type.equalsIgnoreCase("hamster")) {
                Hamster hamsters = new Hamster(listAfter[0], Integer.parseInt(listAfter[1]));
                listHamster.add(hamsters);
            } else if (type.equalsIgnoreCase("parrot")) {
                Parrot parrots = new Parrot(listAfter[0], Integer.parseInt(listAfter[1]));
                listParrot.add(parrots);
            } else if (type.equalsIgnoreCase("lion")) {
                Lion singa = new Lion(listAfter[0], Integer.parseInt(listAfter[1]));
                listLion.add(singa);
            } else if (type.equalsIgnoreCase("eagle")) {
                Eagle elang = new Eagle(listAfter[0], Integer.parseInt(listAfter[1]));
                listEagle.add(elang);
            }
        }
    }

    //Method for printing before and after arrange
    private void printArrange() {
        System.out.println("\n");
        System.out.println("=============================================\n" +
                "Cage arrangement:");
        if (listCat.size() != 0) {
            Cages catCage = new Cages(listCat);
            catCage.arrange();
            catCage.afterArrange();
        }
        if (listLion.size() != 0) {
            Cages lionCage = new Cages(listLion);
            lionCage.arrange();
            lionCage.afterArrange();
        }
        if (listEagle.size() != 0) {
            Cages eagleCage = new Cages(listEagle);
            eagleCage.arrange();
            eagleCage.afterArrange();
        }
        if (listParrot.size() != 0) {
            Cages parrotCage = new Cages(listParrot);
            parrotCage.arrange();
            parrotCage.afterArrange();
        }
        if (listHamster.size() != 0) {
            Cages hamsterCage = new Cages(listHamster);
            hamsterCage.arrange();
            hamsterCage.afterArrange();
        }
    }

    //visit method
    private void visitAnimal() {
        while (true) {
            System.out.print("Which animal you want to visit?\n" +
                    "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)\n");
            Scanner input = new Scanner(System.in);
            String temp = input.nextLine();
            boolean avaibility = false;

            if (temp.equalsIgnoreCase("1")) {
                System.out.print("Mention the name of cat you want to visit: ");
                String tempName = input.nextLine();
                for (Animal tempAnimal : listCat) {
                    if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (cat) now, what would you like to do?\n" +
                                "1: Brush the fur 2: Cuddle");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.action(tempNumber);
                        avaibility = true;
                    }
                }
                if (!avaibility) {
                    System.out.println("There is no cat with that name! Back to the office! \n");
                }
            } else if (temp.equalsIgnoreCase("2")) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String tempName = input.nextLine();
                for (Animal tempAnimal : listEagle) {
                    if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (eagle) now, what would you like to do?\n" +
                                "1: Order to fly");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.action(tempNumber);
                        avaibility = true;
                    }
                }
                if (!avaibility) {
                    System.out.println("There is no eagle with that name! Back to the office! \n");
                }
            } else if (temp.equalsIgnoreCase("3")) {
                System.out.print("Mention the name of Hamster you want to visit: ");
                String tempName = input.nextLine();
                for (Animal tempAnimal : listHamster) {
                    if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (hamster) now, what would you like to do?\n" +
                                "1: See it gnawing 2: Order to run in the hamster wheel");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.action(tempNumber);
                        avaibility = true;
                    }
                }
                if (!avaibility) {
                    System.out.println("There is no hamster with that name! Back to the office! \n");
                }
            } else if (temp.equalsIgnoreCase("4")) {
                System.out.print("Mention the name of Parrot you want to visit: ");
                String tempName = input.nextLine();
                for (Animal tempAnimal : listParrot) {
                    if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (parrot) now, what would you like to do?\n" +
                                "1: Order to fly 2: Do conversation");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.action(tempNumber);
                        avaibility = true;
                    }
                }
                if (!avaibility) {
                    System.out.println("There is no parrot with that name! Back to the office! \n");
                }
            } else if (temp.equalsIgnoreCase("5")) {
                System.out.print("Mention the name of Lion you want to visit: ");
                String tempName = input.nextLine();
                for (Animal tempAnimal : listLion) {
                    if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (lion) now, what would you like to do?\n" +
                                "1: See it hunting 2: Brush the mane 3: Disturb it");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.action(tempNumber);
                        avaibility = true;
                    }
                }
                if (!avaibility) {
                    System.out.println("There is no lion with that name! Back to the office! \n");
                }
            } else {
                break;
            }
        }
    }
}
