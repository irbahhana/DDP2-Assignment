class OutdoorCage extends Animal {

    OutdoorCage(String name, int length) {
        super(name, length);
        this.place = "outdoor";

        //cage code
        if (this.getLength() < 75) {
            this.cageCode = 'A';
        } else if (this.getLength() <= 90) {
            this.cageCode = 'B';
        } else {
            this.cageCode = 'C';
        }
    }

}
