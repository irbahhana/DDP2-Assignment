class IndoorCage extends Animal {
    IndoorCage(String name, int length) {
        super(name, length);
        this.place = "indoor";

        //cage code
        if (this.getLength() < 45) {
            this.cageCode = 'A';
        } else if (this.getLength() <= 60) {
            this.cageCode = 'B';
        } else {
            this.cageCode = 'C';
        }
    }
}
