class Lion extends OutdoorCage {

    Lion(String name, int length) {
        super(name, length);
    }

    //Method action
    public void action(int number) {
        if (number == 1) {
            this.seeItHunting();
        } else if (number == 2) {
            this.brushTheMane();
        } else if (number == 3) {
            this.disturbIt();
        } else {
            System.out.println(this.getName() + " says: HM?");
        }
        System.out.println("Back to the office!\n");
    }

    private void seeItHunting() {
        System.out.println("Lion is hunting..\n" + this.getName() + " makes a voice: err...!");
    }

    private void brushTheMane() {
        System.out.println("Clean the lion’s mane.." + this.getName() + " makes a voice: Hauhhmm!");
    }

    private void disturbIt() {
        System.out.println(this.getName() + " makes a voice: HAUHHMM!");
    }
}
