package javari.animal;
public class Reptile extends Animal{
  private boolean istame =false;

  public Reptile(Integer id, String type, String name, Gender gender, double length,
                double weight,String istame,Condition condition){
                  super(id,type,name,gender,length,weight,condition);
                  if(istame.equals("tame")){
                    this.istame = true;
                  }
                }

  public boolean specificCondition(){
    // Special condition for reptile
    // when tame can perform
    if(istame){
      return true;
    }
    else{
      return false;
    }
  }

}
