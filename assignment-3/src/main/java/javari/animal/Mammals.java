package javari.animal;
public class Mammals extends Animal{
  private boolean ispregnant =false ;

  public Mammals(Integer id, String type, String name, Gender gender, double length,
                double weight,String ispregnant,Condition condition){
                  super(id,type,name,gender,length,weight,condition);
                  if(ispregnant.equals("pregnant")){
                    this.ispregnant = true;
                  }
                }

  public boolean specificCondition(){ 
    // Special condition when mamal is pregnant
    //  they cant perform   
    if(getType().equals("Lion")){
      if(getGender()==Gender.MALE && !ispregnant){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      if(!ispregnant){
        return true;
      }
      else{
        return false;
      }
    }
  }

}
