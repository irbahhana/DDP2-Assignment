package javari.reader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

  
public class ReaderAttractions extends CsvReader{
  private List<String> attractions = new ArrayList<String>();
  public ReaderAttractions(Path File)throws IOException{
    super(File);
  }
  public long countValidRecords(){
    for(String record : lines){
      String[] data = record.split(COMMA);
      try{
        for(String attraction : attractions){
          if(data[1].equals(attraction)){
            throw new Exception();
          }
        }
        attractions.add(data[1]);
      }
      catch(Exception e){
        continue;
      }

  }

    return (long) attractions.size();
  }

  public long countInvalidRecords(){
    long invalid =0;
    for(String record:lines){
      String[] data = record.split(COMMA);
      if(data.length!=2){
        invalid+=1;
      }
    }
    return invalid;
  }

  public List<String> getAttractions(){
    return attractions;
  }

}
