package javari.reader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
public class ReaderSections extends CsvReader{
  private List<String> sections = new ArrayList<String>();
  public ReaderSections(Path File)throws IOException{
    super(File);
  }
  public long countValidRecords(){
    for(String record : lines){
      String[] data = record.split(COMMA);
      try{
        for(String section : sections){
          if(data[2].equals(section)){
            throw new Exception();
          }
        }
        sections.add(data[2]);
      }
      catch(Exception e){
        continue;
      }

  }

    return (long) sections.size();
  }

  public long countInvalidRecords(){
    long invalid =0;
    for(String record:lines){
      String[] data = record.split(COMMA);
      if(data.length!=3){
        invalid+=1;
      }
    }
    return invalid;
  }

  public List<String> getSections(){
    return sections;
  }

}
