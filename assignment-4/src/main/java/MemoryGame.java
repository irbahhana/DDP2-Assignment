/**
 * Author   = Muhammad Naufal Irbahhana
 * Class    = DDP2 KI CSUI
 * this code was meant for Assignment and entertainment purposes only. Using this as a reference for future coding is
 * allowed.
 */

import panel.MemoryPanel;
import javax.swing.*;

/**
 * This is the Main class. This class contain GUI code.
 * The program is using Java Swing, this class contained Frame and Panels to make the game appear for users to play.
 * it uses panel for better code grouping and easy use.
 */
public class MemoryGame {
    public MemoryGame() {
        JFrame frame = new JFrame("Match-Pair Memory Game");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(720 ,660);
        MemoryPanel panel = new MemoryPanel();
        frame.getContentPane().add(panel.getMainPanel());
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Kata pa daya ikutin aja ini, biar Swing nya bisa jalan hehehe
     * refrence : http://csui.cozora.com/course/learning?id=6&materialid=0
     * @param args
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MemoryGame game = new MemoryGame();
            }
        });
    }
}
